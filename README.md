# Test rolling-release application

Dependencies:
* `Node.js binary >= 7.0.0`

Installation:
1. Clone this repository, each release version is tagged with `vX.X.X`, i.e. `v1.0.0`
2. Fetch dependencies with `npm install`
3. Ensure all required environmental variables defined in `.env.example` are present
4. Run `node index.js` or `npm start`

Available versions:
1. `v1.0.0` - Initial release
2. `v1.1.0` - Simulate a failed release, process will exit after one second
3. `v2.0.0` - Simulate a fixed release, everything back to normal
