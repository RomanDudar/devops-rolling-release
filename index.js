require('dotenv-safe').load();

const appPackage = require('./package.json');

const http = require('http');
const express = require('express');

const gracefulShutdown = require('./gracefulShutdown');

const app = express();
const server = http.createServer(app);

app.use(gracefulShutdown(server, {
  forceTimeout: 3 * 1000
}));

app.get('*', (req, res) => {
  const delay = Math.random() * 2000;
  setTimeout(() => {
    res.json({
      app: process.env.APP_KEY,
      version: appPackage.version,
      servedIn: delay
    });
  }, delay);
});

// Simulate startup delay
setTimeout(() => {
  server.listen(process.env.APP_PORT, (err) => {
    if (err) {
      console.log(`Couldn't start on ${process.env.APP_PORT}: ${err}`);
    }

    console.log(`Listening on ${process.env.APP_PORT}`);
  });
}, 1000);
